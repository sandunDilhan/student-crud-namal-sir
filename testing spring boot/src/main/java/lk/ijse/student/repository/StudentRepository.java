package lk.ijse.student.repository;

import lk.ijse.student.entity.StudentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
@Repository
public interface StudentRepository extends CrudRepository<StudentEntity,Integer>{

}
