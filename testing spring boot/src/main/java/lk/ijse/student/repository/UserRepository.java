package lk.ijse.student.repository;

import lk.ijse.student.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by sandunDilhan on 8/29/2018.
 */
@Repository
public interface UserRepository extends CrudRepository<UserEntity,String> {

    @Query(value = "select * from user_entity where name=:uName and password=:pw",nativeQuery = true)
    UserEntity checkPassword(@Param("uName") String uName, @Param("pw") String pw);
}
