package lk.ijse.student.controller;

import lk.ijse.student.dto.UserDTO;
import lk.ijse.student.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by sandunDilhan on 8/29/2018.
 */
@CrossOrigin
@RestController
@RequestMapping("api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/{name}/{pw}")
    public UserDTO checkPassword(@PathVariable("name") String name,@PathVariable("pw") String pw){
        return userService.checkPassword(name,pw);
    }

//    @GetMapping
//    public Object findAllCustomers(@RequestParam(value = "action", required = false) String action
//            ,@RequestParam(value="name", required = false) String name){
//        if (action !=null){
//            switch (action){
//                case "count":
//                    return service.getCustomersCount();
//                case "like":
//                    return service.findCustomersLike(name);
//                default:
//                    return service.findAllCustomers();
//            }
//        }else {
//            return service.findAllCustomers();
//        }
//    }
}
