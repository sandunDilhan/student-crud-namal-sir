package lk.ijse.student.controller;

import lk.ijse.student.dto.StudentDTO;
import lk.ijse.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
@CrossOrigin
@RestController
@RequestMapping("api/v1/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PutMapping
    public String saveStudent(@RequestBody StudentDTO studentDTO){
        studentService.saveStudent(studentDTO);
        return "student save";
    }

    @PostMapping("/{id}")
    public String updateStudent(@PathVariable("id") int studentId,@RequestBody StudentDTO studentDTO){
        studentService.updateStudent(studentId,studentDTO);
        return "update success";
    }

    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable("id") int studentId){
        studentService.deleteStudent(studentId);
        return "delete success";
    }

    @GetMapping("/{id}")
    public StudentDTO findByStudent(@PathVariable("id") int studentId){
        return studentService.findStudent(studentId);
    }

    @GetMapping
    public List<StudentDTO> findAllStudent(){
        return studentService.findAllStudent();
    }
}
