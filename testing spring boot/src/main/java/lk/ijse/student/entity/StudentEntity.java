package lk.ijse.student.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
@Entity
public class StudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int studentId;
    private String studentName;
    private String address;
    private String gender;
    private String mobileNo;

    public StudentEntity() {
    }

    public StudentEntity(String studentName, String address, String gender, String mobileNo) {
        this.studentName = studentName;
        this.address = address;
        this.gender = gender;
        this.mobileNo = mobileNo;
    }

    public StudentEntity(int studentId, String studentName, String address, String gender, String mobileNo) {
        this.studentId=studentId;
        this.studentName = studentName;
        this.address = address;
        this.gender = gender;
        this.mobileNo = mobileNo;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Override
    public String toString() {
        return "StudentEntity{" +
                "studentId=" + studentId +
                ", studentName='" + studentName + '\'' +
                ", address='" + address + '\'' +
                ", gender='" + gender + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                '}';
    }
}
