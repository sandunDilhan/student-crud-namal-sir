package lk.ijse.student.service;

import lk.ijse.student.dto.StudentDTO;

import java.util.List;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
public interface StudentService {
    void saveStudent(StudentDTO dto);

    void updateStudent(int studentId, StudentDTO dto);

    void deleteStudent(int studentId);

    StudentDTO findStudent(int studentId);

    List<StudentDTO> findAllStudent();
}
