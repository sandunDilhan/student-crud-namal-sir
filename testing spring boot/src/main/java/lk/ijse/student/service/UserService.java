package lk.ijse.student.service;

import lk.ijse.student.dto.UserDTO;

/**
 * Created by sandunDilhan on 8/29/2018.
 */
public interface UserService {

    public UserDTO checkPassword(String name,String id);
}
