package lk.ijse.student.service.impl;

import lk.ijse.student.dto.UserDTO;
import lk.ijse.student.entity.UserEntity;
import lk.ijse.student.repository.UserRepository;
import lk.ijse.student.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sandunDilhan on 8/29/2018.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

//    check user using user name and pw
    @Override
    public UserDTO checkPassword(String name, String pw) {
        UserEntity userEntity=userRepository.checkPassword(name, pw);
        return new UserDTO(userEntity.getName(),userEntity.getPassword());
    }
}
