package lk.ijse.student.service.impl;

import lk.ijse.student.dto.StudentDTO;
import lk.ijse.student.entity.StudentEntity;
import lk.ijse.student.repository.StudentRepository;
import lk.ijse.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void saveStudent(StudentDTO dto) {
        studentRepository.save(new StudentEntity(dto.getStudentName(),dto.getAddress(),dto.getGender(),dto.getMobileNo()));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateStudent(int studentId, StudentDTO dto) {
        if(dto.getStudentId()!=studentId){
            throw new RuntimeException("Student ID mismatched");
        }
        if (studentRepository.existsById(studentId)){
            studentRepository.save(studentRepository.save(new StudentEntity(dto.getStudentId(),dto.getStudentName(),dto.getAddress(),dto.getGender(),dto.getMobileNo())));
        }else{
            throw new RuntimeException("Student doesn't exist");
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void deleteStudent(int studentId) {
        studentRepository.deleteById(studentId);
    }

    @Override
    public StudentDTO findStudent(int studentId) {
        StudentEntity studentEntity=studentRepository.findById(studentId).get();
        return new StudentDTO(studentEntity.getStudentId(),studentEntity.getStudentName(),studentEntity.getAddress(),studentEntity.getGender(),studentEntity.getMobileNo());
    }

    @Override
    public List<StudentDTO> findAllStudent() {
        Iterable<StudentEntity>getAllStudent=studentRepository.findAll();
        List<StudentDTO>studentDTOList=new ArrayList<>();
        for(StudentEntity studentEntity:getAllStudent){
            studentDTOList.add(new StudentDTO(studentEntity.getStudentId(),studentEntity.getStudentName(),studentEntity.getAddress(),studentEntity.getGender(),studentEntity.getMobileNo()));
        }
        return studentDTOList;
    }
}
