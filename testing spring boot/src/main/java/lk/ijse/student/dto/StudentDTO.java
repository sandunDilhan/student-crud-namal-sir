package lk.ijse.student.dto;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
public class StudentDTO {
    private int studentId;
    private String studentName;
    private String address;
    private String gender;
    private String mobileNo;

    public StudentDTO() {
    }

    public StudentDTO(int studentId, String studentName, String address, String gender, String mobileNo) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.address = address;
        this.gender = gender;
        this.mobileNo = mobileNo;
    }

    public StudentDTO(String studentName, String address, String gender, String mobileNo) {
        this.studentName = studentName;
        this.address = address;
        this.gender = gender;
        this.mobileNo = mobileNo;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
