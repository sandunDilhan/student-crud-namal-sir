package lk.ijse.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by sandunDilhan on 8/28/2018.
 */
@SpringBootApplication
public class main {
    public static void main(String[] args) {
        SpringApplication.run(main.class);
    }
}
