<%--
  Created by IntelliJ IDEA.
  User: sandunDilhan
  Date: 8/19/2018
  Time: 9:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../css/style.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">

    <title>Student</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
</head>
<script>
//    load navigation bar-----------------------------------------------------------------------------------
    $(function () {
        $("#includedNavigationBar").load("../includes/navigationBar.jsp");
    });

    function viewItemForm() {
        document.getElementById("createStudentDivId").style.display="block";
        document.getElementById("updateBtn").style.display="none";
        document.getElementById("saveBtn").style.display="block";
        document.getElementById("txtStudentID").readOnly=true;
        document.getElementById("txtStudentName").value="";
        document.getElementById("txtStudentAddress").value="";
        document.getElementById("txtStudentMobile").value="";

    }

    function cancelItemForm() {
        document.getElementById("createStudentDivId").style.display="none";
    }

//    load all student in student table-----------------------------------------------------------------
    function loadAllStudent() {
        var ajaxConfig={
            method:"GET",
            url:"http://localhost:8080/api/v1/students",
            async:true
        }

//        print response in student table
        $.ajax(ajaxConfig).done(function (response) {
           response.forEach(function (student) {
               var html="<tr>" +
                       "<td>" + student.studentId + "</td>" +
                       "<td>" + student.studentName + "</td>" +
                       "<td>" + student.address + "</td>" +
                       "<td>" + student.gender + "</td>" +
                       "<td>" + student.mobileNo + "</td>" +
                       '<td>' +
                       '<button class="btn btn-outline-warning" type="button" onclick="loadSudentUpdateText()">edit</button>' +
                       '<button class="btn btn-outline-danger" type="button" onclick="removeStudent()">remove</button>' +
                       '</td>' +
                       "</tr>";

               $("#tblStudent tbody").append(html);
           })
        });
    }

//    student save--------------------------------------------------------------------------
    function saveStudent() {
        var sName=document.getElementById("txtStudentName").value;
        var sAddress=document.getElementById("txtStudentAddress").value;
        var sGender=document.getElementById("cmbStudentGender").value;
        var sMobile=document.getElementById("txtStudentMobile").value;

//        create json object
        var addStudent={
            "studentId":0,
            "studentName":sName,
            "address":sAddress,
            "gender":sGender,
            "mobileNo":sMobile
        }


        var x=confirm("Are you sure!!! you want to save " +sName+" ?");
        if(x===true){
            var ajaxConfig={
                method:"PUT",
                contentType : 'application/json; charset=utf-8',
//                dataType : 'json',
                url:"http://localhost:8080/api/v1/students",
                data:JSON.stringify(addStudent),
                async:true
            }

            $.ajax(ajaxConfig).done(function (response) {
                if(response==="student save"){
                    alert("Student has been successfully save");
                    window.location.href="index.jsp";
                }
            });
        }
    }

//    delete selected student-----------------------------------------------------------------------------------------
    function removeStudent() {
        var table=document.getElementById("tblStudent");

        for(var i = 1; i < table.rows.length; i++)
        {
            table.rows[i].onclick = function() {
//                get student id
                var sId = this.cells[0].innerHTML;
                var x=confirm("Are you sure!!! you want to remove "+sId+" ?")
                if(x===true){
                    var ajaxConfig={
                        method: "DELETE",
                        url:"http://localhost:8080/api/v1/students/"+sId,
                        async:true
                    }

                    $.ajax(ajaxConfig).done(function (response) {
                        if(response==="delete success"){
                            alert("Student has been successfully deleted");
                            window.location.href="index.jsp";
                        }else{
                            alert("Student has been remove failed");
                        }
                    });
                }
            };
        }
    }

//    click edit button and fill all student detail in createStudent div-----------------------------------------------------
    function loadSudentUpdateText() {
        var table = document.getElementById('tblStudent');
        for(var i=1;i<table.rows.length;i++){
            table.rows[i].onclick=function () {
                document.getElementById("txtStudentID").value=this.cells[0].innerHTML;
                document.getElementById("txtStudentName").value=this.cells[1].innerHTML;
                document.getElementById("txtStudentAddress").value=this.cells[2].innerHTML;
                document.getElementById("cmbStudentGender").value=this.cells[3].innerHTML;
                document.getElementById("txtStudentMobile").value=this.cells[4].innerHTML;
            }
        }
//        style update text field-------------------------------------------------------------------------------
        document.getElementById("createStudentDivId").style.display="block";
        document.getElementById("saveBtn").style.display= "none";
        document.getElementById("updateBtn").style.display= "block";
        document.getElementById("txtStudentID").readOnly = true;
    }

//    update student detail--------------------------------------------------------------------------------------
    function updateStudent() {

//        get selected student detail

        var sId=document.getElementById("txtStudentID").value;
        var sName=document.getElementById("txtStudentName").value;
        var sAddress=document.getElementById("txtStudentAddress").value;
        var sGender=document.getElementById("cmbStudentGender").value;
        var sMobile=document.getElementById("txtStudentMobile").value;

//        create json object

        var updateCustomer={
            "studentId":sId,
            "studentName":sName,
            "address":sAddress,
            "gender":sGender,
            "mobileNo":sMobile
        }

        var x=confirm("Are you sure!!! you want to update "+sId+" ?");
        if(x===true){
            var ajaxConfig={
                method:"POST",
                contentType : 'application/json; charset=utf-8',
//                dataType : 'json',
                url:"http://localhost:8080/api/v1/students/"+sId,
                data:JSON.stringify(updateCustomer),
                async:true
            };

            $.ajax(ajaxConfig).done(function (response) {
                if(response==="update success"){
                    alert("Student has been successfully update");
                    window.location.href="index.jsp";
                }
            });
        }
    }

</script>
<body class="animated fadeIn delay-5s" style="background: linear-gradient(to right, #f7f8f8, #acbb78);" onload="loadAllStudent()">
<div id="includedNavigationBar"></div>

<div class="container">
    <table class="table table-sm table-hover animated bounce delay-5s" id="tblStudent" style="margin-top: 11%;" onload="loadAllStudent()">
        <thead style="background-color: cornflowerblue">
        <tr>
            <th>Student Id</th>
            <th>Full Name</th>
            <th>Address</th>
            <th>Gender</th>
            <th>mobile No.</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<div class="createItem">
    <button class="btn btn-outline-primary" type="button" style="width: 100%;color: white;background: #2b5876;" onclick="viewItemForm()"><b>create</b></button>
</div>

<div  class="createItemDiv animated rotateInDownLeft delay-5s" id="createStudentDivId">
    <ul class="createItemUl">
        <br>
        <div class="row">
            <button class="btn btn-danger" style="margin-left: 86%;width: 92px;" type="button" onclick="cancelItemForm()">X</button>
        </div>
        <input hidden type="text" style="margin-left: 14%;width: 60%;" id="txtStudentID">
        <br>
        <div class="row">
            <p class="itemP">Full Name</p>
            <input type="text" style="margin-left: 14%;width: 60%;" id="txtStudentName">
        </div>
        <br>
        <div class="row">
            <p class="itemP">Address</p>
            <input type="text" style="margin-left: 15.8%" id="txtStudentAddress">
        </div>
        <br>
        <div class="row">
            <p class="itemP">Gender</p>
            <select type="text" style="margin-left: 16.4%" id="cmbStudentGender">
                <option>Male</option>
                <option>female</option>
            </select>
        </div>
        <br>
        <div class="row">
            <p class="itemP">mobile No.</p>
            <input type="text" style="margin-left: 13.1%" id="txtStudentMobile">
        </div>
        <br>
        <div class="row">
            <button class="btn btn-success" style="margin-left: 86%;width: 92px;" type="button" id="saveBtn" onclick="saveStudent()">save</button>
            <button class="btn btn-warning" style="margin-left: 86%;width: 92px; margin-top: -4.5%;height: 37px;" id="updateBtn" type="button" onclick="updateStudent()">update</button>
        </div>
        <br>
    </ul>
</div>
</body>
</html>